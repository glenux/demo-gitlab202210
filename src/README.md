# Enoncé

Dans le fichier `ELEVES.md` vous ajouterez votre nom, sous la forme suivante :

```
* NOM, Prénom
```

... en vous assurant que les noms sont bien classés dans l'ordre alphabétique
des noms de famille.

